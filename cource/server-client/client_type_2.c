#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>     /* for close() */
#include "constants.h"


int main(int argc, char *argv[])
{
	int cli_type=2;
	int sockfd;
	int sock;                       /* Socket descriptor */
	int* size;
	int* time;
	struct sockaddr_in ServAddr; 	/* Echo server address */
	char buf[21];
	char welcome[21]="Queue have messages";
	char* mess;
	struct sockaddr_in sendaddr;
	struct sockaddr_in recvaddr;
	int numbytes;
	int addr_len;
	int broadcast=1;
	srand(getpid());
	if((sockfd = socket(PF_INET, SOCK_DGRAM, 0)) == -1)
	{
		perror("socket");
		exit(1);
	}
    printf("Socket created\n");
    if((setsockopt(sockfd,SOL_SOCKET,SO_BROADCAST,
                                &broadcast,sizeof broadcast)) == -1)
    {
		perror("setsockopt - SO_SOCKET ");
		exit(1);
    }
    printf("Socket options setted\n");
 
    sendaddr.sin_family = AF_INET;
    sendaddr.sin_port = htons(UDPPORT);
    sendaddr.sin_addr.s_addr = INADDR_ANY;
    memset(sendaddr.sin_zero,'\0', sizeof sendaddr.sin_zero);
 
 
    recvaddr.sin_family = AF_INET;
    recvaddr.sin_port = htons(UDPPORT);
    recvaddr.sin_addr.s_addr = INADDR_ANY;
    memset(recvaddr.sin_zero,'\0',sizeof recvaddr.sin_zero);
    if(bind(sockfd, (struct sockaddr*) &recvaddr, sizeof recvaddr) == -1)
    {
        perror("bind");
        exit(1);
    }
        
    printf("Socket bind success\n");
    
 
    addr_len = sizeof sendaddr;

	memset(&ServAddr, 0, sizeof(ServAddr));     /* Zero out structure */
	ServAddr.sin_family      = AF_INET;             /* Internet address family */
	ServAddr.sin_port        = htons(TCPPORT); /* Server port */
	int que_cur_size;
	while  (1) 
	{
		if ((numbytes = recvfrom(sockfd, buf, sizeof buf, 0,
              (struct sockaddr *)&sendaddr, (socklen_t *)&addr_len)) == -1)
		{
			perror("recvfrom");
			exit(1);
		}
		else {
		char* servIP = inet_ntoa(sendaddr.sin_addr);
		printf("From IP: %s\n", servIP);
		printf("Recieved: %s\n",buf);
		ServAddr.sin_addr.s_addr = inet_addr(servIP);   /* Server IP address */
		if (strcmp(buf,welcome)==0){
		if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
			DieWithError("socket() failed");
		/* Establish the connection to the echo server */
		if (connect(sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0)
		{
			DieWithError("connect() failed");
		}
		printf("Connection established\n");
		send (sock, &cli_type, sizeof(cli_type), 0);
		printf("Client type sent\n");
		recv(sock, &que_cur_size, sizeof(int), 0);
		printf("Current size of queue is %d\n", que_cur_size);
		size=malloc(sizeof(int));
		recv(sock, size, sizeof(int), 0);
		printf("Recieved size of message: %d\n", *size);
		if((mess=malloc(*size))){
			printf("Memory allocated\n");
		}
		time=malloc(sizeof(int));
		recv(sock, time, sizeof(int), 0);
		printf("Recieved maintanace time: %d\n", *time);
		recv(sock, mess, *size, 0);
		printf("Recieved message: %s\n",mess);
		close(sock);
		printf("Connection closed\n");
		printf("Sleeping for %d sec\n", *time);
		sleep(*time);	
		free(mess);
		free(size);
		free(time);
	}	
	//close(sockfd);
	}
	strcpy(buf,"");
	}
    return 0;
}
