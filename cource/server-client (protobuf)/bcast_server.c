#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include "constants.h"
#include "netmes.pb-c.h"
#include "netmes.pb-c.c"


pthread_mutex_t queue_lock = PTHREAD_MUTEX_INITIALIZER;
int i_m=0;

struct UDPs {
	struct sockaddr_in recvaddr;
	int sockfd;
};

struct queue_str {
	char* cell[MAX_MESS];
	int size[MAX_MESS];
	int time[MAX_MESS];
	int sock;
};

struct queue_buf {
	uint8_t* cell[MAX_MESS];
	unsigned len[MAX_MESS];
	int sock;
};


void* UDP_message_handler_1 (void *args)
{
	printf("UDP handler 1 started\n");
	extern int i_m;
	char wait[21] = "Waiting for messages\0";
	struct UDPs* udp = (struct UDPs*) args; 
	int sockfd=udp->sockfd;
	struct sockaddr_in recvaddr = udp->recvaddr;
   while(1)
   {
	   if (i_m<MAX_MESS)
	   {
		   sendto(sockfd, wait, sizeof wait , 0,(struct sockaddr *)&recvaddr, sizeof recvaddr);
		   printf("Sent welcome message\n");
	   }
	   sleep(W_TIME);
   }
   perror("sendto");
   close(sockfd);
   exit(1);
   pthread_exit(NULL);
}

void* UDP_message_handler_2 (void *args)
{
	printf("UDP handler 2 started\n");
	extern int i_m;
	char full[21] = "Queue have messages\0";
	struct UDPs* udp = (struct UDPs*) args; 
	int sockfd=udp->sockfd;
	struct sockaddr_in recvaddr = udp->recvaddr;
	
   
   while(1)
   {
		if (i_m>0)
		{
		   sendto(sockfd, full, sizeof full , 0,(struct sockaddr *)&recvaddr, sizeof recvaddr);
		   printf("Sent filled message\n");
	   }	
	   sleep(F_TIME);
   }
   perror("sendto");
   close(sockfd);
   exit(1);
   pthread_exit(NULL);
}

void* client_handler2 (void *args)
{
	printf("Client handler started\n");
	extern int i_m;
	struct queue_buf* queue = (struct queue_buf*) args;
	Netmes *msg; 	
	int sock = queue->sock;
	int cli_type;
	pthread_mutex_lock(&queue_lock);
	printf("mutex locked\n");
	recv(sock, &cli_type, sizeof(int), 0);
	send(sock, &i_m, sizeof(int), 0);
	if ((cli_type==1)&&(i_m<MAX_MESS))
	{
		printf("client type determined as 1\n");
		recv(sock, &queue->len[i_m], sizeof(unsigned), 0);
		printf("%d is len\n", queue->len[i_m]);
		queue->cell[i_m]=malloc(queue->len[i_m]);
		printf("memory is given\n");
		recv(sock, queue->cell[i_m], queue->len[i_m], 0);
		msg=netmes__unpack(NULL, queue->len[i_m], queue->cell[i_m]);
		printf("%s is stored\n", msg->message);
		printf("%d its maintenance time\n", msg->time);
		i_m++;
		printf("%d now is current size of queue\n", i_m);
	}
	
	if ((cli_type==2)&&(i_m>0))
	{
		printf("client type determined as 2\n");
		//printf("%d its size\n", queue->size[0]);
		send(sock, &queue->len[0], sizeof(unsigned), 0);
		send(sock, queue->cell[0], queue->len[0], 0);
		for (int i=0; i<i_m-1; i++)
		{
			strcpy(queue->cell[i],queue->cell[i+1])  ;
		}
		free(queue->cell[i_m-1]);
		i_m--;
		printf("%d now is current size of queue\n", i_m);
	}
	pthread_mutex_unlock(&queue_lock);
	printf("mutex unlocked\n");	
	close(sock);
	pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
	struct queue_buf queue;
	//struct queue_buf msg;
	pthread_t udp_handler_1,udp_handler_2, temp;
	int sock;
	struct sockaddr_in echoServAddr; // Local address 
	
	int broadcast=1;
    struct sockaddr_in sendaddr;
    struct UDPs udp;
    if((udp.sockfd = socket(PF_INET,SOCK_DGRAM,IPPROTO_UDP)) == -1)
    {
        perror("sockfd");
        exit(1);
    }
    printf("Socket for UDP created\n");
 
   if((setsockopt(udp.sockfd,SOL_SOCKET,SO_BROADCAST,
                                &broadcast,sizeof broadcast)) == -1)
   {
        perror("setsockopt - SO_SOCKET ");
        exit(1);
   }
   printf("Socket for UDP options setted\n");
   sendaddr.sin_family = AF_INET;
   sendaddr.sin_port = htons(UDPPORT);
   sendaddr.sin_addr.s_addr = INADDR_ANY;
   memset(sendaddr.sin_zero,'\0',sizeof sendaddr.sin_zero);
 
   udp.recvaddr.sin_family = AF_INET;
   udp.recvaddr.sin_port = htons(UDPPORT);
   udp.recvaddr.sin_addr.s_addr = inet_addr(DEST_ADDR);
   memset(udp.recvaddr.sin_zero,'\0',sizeof udp.recvaddr.sin_zero);
   
	if(pthread_create( &udp_handler_1, NULL, UDP_message_handler_1, &udp) < 0)
	{
		perror("could not create thread");
		return 1;
	}
	if(pthread_create( &udp_handler_2, NULL, UDP_message_handler_2, &udp) < 0)
	{
		perror("could not create thread");
		return 1;
	}
	
	
	if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
	DieWithError("socket() failed");
	// Construct local address structure
	memset(&echoServAddr, 0, sizeof(echoServAddr));   //Zero out structure 
	echoServAddr.sin_family = AF_INET;                //Internet address family
	echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); //Any incoming interface
	echoServAddr.sin_port = htons(TCPPORT);      //Local port 
	if (bind(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0)
		DieWithError("bind() failed");
	listen(sock, 1);
	
	while((queue.sock=accept(sock, NULL, NULL)))
	{
		puts("Connection accepted");
		if(pthread_create( &temp, NULL, client_handler2, &queue) < 0)
        {
            perror("could not create thread");
            return 1;
        }
        ;
	}
	pthread_join(udp_handler_1, NULL);
	pthread_join(udp_handler_2, NULL);
	return 0;
}
