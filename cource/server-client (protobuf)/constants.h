#pragma once
#define UDPPORT 2080
#define TCPPORT 2081
#define DEST_ADDR "255.255.255.255"
#define MESS_SIZE 15 // max size of message
#define MAX_MESS 6 // max number of messages
#define MAX_TIME 15 // max maintenance time
#define W_TIME 2 // time of welcome message
#define F_TIME 8 // time of filled message

 
void DieWithError(char *errorMessage)
{
    perror(errorMessage);
    exit(1);
}
