#include <sys/types.h>
#include <sys/socket.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>     /* for close() */
#include "constants.h"
#include "netmes.pb-c.h"
#include "netmes.pb-c.c"
 
 

struct gen_mess {
	void* pointer;
	int size;
};

void string_generator(struct gen_mess* array) {
	Netmes* msg=array->pointer;
	printf("Message generation process: \n");
	array->size=2+rand()%(MESS_SIZE-2);
	printf("\t Size of message: %d\n",array->size);
	msg->time=1+rand()%(MAX_TIME-1);
	printf("\t Maintanance time: %d\n",msg->time);
	msg->message=malloc(array->size);
	for (int i=0;i<array->size-1;i++) msg->message[i]=' '+rand()%96;
	msg->message[array->size-1]='\0';
	printf("\t Message: %s\n",msg->message);
}



int main(int argc, char *argv[])
{
	struct gen_mess gen;
	Netmes msg = NETMES__INIT; 		// Netmes
	void *buf;                     	// Buffer to store serialized data
	unsigned len;                  	// Length of serialized data
	
	int cli_type=1;
	int sockfd;
	int sock;                       /* Socket descriptor */
	struct sockaddr_in ServAddr; 	/* Echo server address */
	char buff[21];
	char welcome[21]="Waiting for messages";
	struct sockaddr_in sendaddr;
	struct sockaddr_in recvaddr;
	int numbytes;
	int addr_len;
	int broadcast=1;
	srand(getpid());
	if((sockfd = socket(PF_INET, SOCK_DGRAM, 0)) == -1)
	{
		perror("socket");
		exit(1);
	}
    printf("Socket created\n");
    if((setsockopt(sockfd,SOL_SOCKET,SO_BROADCAST,
                                &broadcast,sizeof broadcast)) == -1)
    {
		perror("setsockopt - SO_SOCKET ");
		exit(1);
    }
    printf("Socket options setted\n");
 
    sendaddr.sin_family = AF_INET;
    sendaddr.sin_port = htons(UDPPORT);
    sendaddr.sin_addr.s_addr = INADDR_ANY;
    memset(sendaddr.sin_zero,'\0', sizeof sendaddr.sin_zero);
 
 
    recvaddr.sin_family = AF_INET;
    recvaddr.sin_port = htons(UDPPORT);
    recvaddr.sin_addr.s_addr = INADDR_ANY;
    memset(recvaddr.sin_zero,'\0',sizeof recvaddr.sin_zero);
    if(bind(sockfd, (struct sockaddr*) &recvaddr, sizeof recvaddr) == -1)
    {
        perror("bind");
        exit(1);
    }
        
    printf("Socket bind success\n");

    addr_len = sizeof sendaddr;

	memset(&ServAddr, 0, sizeof(ServAddr));     /* Zero out structure */
	ServAddr.sin_family      = AF_INET;             /* Internet address family */
	ServAddr.sin_port        = htons(TCPPORT); /* Server port */
	int que_cur_size;
	while  (1) 
	{
		if ((numbytes = recvfrom(sockfd, buff, sizeof buff, 0,
              (struct sockaddr *)&sendaddr, (socklen_t *)&addr_len)) == -1)
		{
			perror("recvfrom");
			exit(1);
		}
		else {
		char* servIP = inet_ntoa(sendaddr.sin_addr);
		printf("From IP: %s\n", servIP);
		printf("Recieved: %s\n",buff);
		ServAddr.sin_addr.s_addr = inet_addr(servIP);   /* Server IP address */
		printf("%s\n", buff);
		printf("%s\n",welcome);
		if (strcmp(buff,welcome)==0){
		if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
			DieWithError("socket() failed");
		/* Establish the connection to the echo server */
		if (connect(sock, (struct sockaddr *) &ServAddr, sizeof(ServAddr)) < 0)
		{
			DieWithError("connect() failed");
		}
		printf("Connection established\n");
		send (sock, &cli_type, sizeof(cli_type), 0);
		printf("Client type sent\n");
		recv(sock, &que_cur_size, sizeof(int), 0);
		printf("Current size of queue is %d\n", que_cur_size);
		if (que_cur_size!=MAX_MESS) 
		{
			gen.pointer=&msg;
			string_generator(&gen);
			
			len = netmes__get_packed_size(&msg);
			buf = malloc(len);
			netmes__pack(&msg,buf);
			send (sock, &len, sizeof(unsigned), 0);
			send (sock, buf, len, 0);
			free(buf);
		}
		close(sock);
		printf("Connection closed\n");
		printf("Sleeping for %d sec\n", msg.time);
		sleep(msg.time);
		free(msg.message);
	}	
	close(sock);
	}
	strcpy(buff,"");
	}
    return 0;
}
