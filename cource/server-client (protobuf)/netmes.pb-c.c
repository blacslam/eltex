/* Generated by the protocol buffer compiler.  DO NOT EDIT! */
/* Generated from: netmes.proto */

/* Do not generate deprecated warnings for self */
#ifndef PROTOBUF_C__NO_DEPRECATED
#define PROTOBUF_C__NO_DEPRECATED
#endif

#include "netmes.pb-c.h"
void   netmes__init
                     (Netmes         *message)
{
  static const Netmes init_value = NETMES__INIT;
  *message = init_value;
}
size_t netmes__get_packed_size
                     (const Netmes *message)
{
  assert(message->base.descriptor == &netmes__descriptor);
  return protobuf_c_message_get_packed_size ((const ProtobufCMessage*)(message));
}
size_t netmes__pack
                     (const Netmes *message,
                      uint8_t       *out)
{
  assert(message->base.descriptor == &netmes__descriptor);
  return protobuf_c_message_pack ((const ProtobufCMessage*)message, out);
}
size_t netmes__pack_to_buffer
                     (const Netmes *message,
                      ProtobufCBuffer *buffer)
{
  assert(message->base.descriptor == &netmes__descriptor);
  return protobuf_c_message_pack_to_buffer ((const ProtobufCMessage*)message, buffer);
}
Netmes *
       netmes__unpack
                     (ProtobufCAllocator  *allocator,
                      size_t               len,
                      const uint8_t       *data)
{
  return (Netmes *)
     protobuf_c_message_unpack (&netmes__descriptor,
                                allocator, len, data);
}
void   netmes__free_unpacked
                     (Netmes *message,
                      ProtobufCAllocator *allocator)
{
  if(!message)
    return;
  assert(message->base.descriptor == &netmes__descriptor);
  protobuf_c_message_free_unpacked ((ProtobufCMessage*)message, allocator);
}
static const ProtobufCFieldDescriptor netmes__field_descriptors[2] =
{
  {
    "message",
    1,
    PROTOBUF_C_LABEL_REQUIRED,
    PROTOBUF_C_TYPE_STRING,
    0,   /* quantifier_offset */
    offsetof(Netmes, message),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
  {
    "time",
    2,
    PROTOBUF_C_LABEL_REQUIRED,
    PROTOBUF_C_TYPE_INT32,
    0,   /* quantifier_offset */
    offsetof(Netmes, time),
    NULL,
    NULL,
    0,             /* flags */
    0,NULL,NULL    /* reserved1,reserved2, etc */
  },
};
static const unsigned netmes__field_indices_by_name[] = {
  0,   /* field[0] = message */
  1,   /* field[1] = time */
};
static const ProtobufCIntRange netmes__number_ranges[1 + 1] =
{
  { 1, 0 },
  { 0, 2 }
};
const ProtobufCMessageDescriptor netmes__descriptor =
{
  PROTOBUF_C__MESSAGE_DESCRIPTOR_MAGIC,
  "Netmes",
  "Netmes",
  "Netmes",
  "",
  sizeof(Netmes),
  2,
  netmes__field_descriptors,
  netmes__field_indices_by_name,
  1,  netmes__number_ranges,
  (ProtobufCMessageInit) netmes__init,
  NULL,NULL,NULL    /* reserved[123] */
};
