#! /bin/bash
SHAREDIR=/home/nfs
MYIP=$(hostname -I)
MYDIR=/home/slam/share
#echo "Enter remote IP"
#read REIP
REIP=192.168.101.167
LEN=${#MYIP}
let "LEN=LEN-1"
MYIP=${MYIP:0:$LEN}

if [[ "$(mount -t nfs4 | grep "$MYDIR" -c)" -eq NULL ]]
then 
echo "not mounted, mounting now"
mount -t nfs -O iocharset=utf-8 $REIP:$SHAREDIR $MYDIR
sleep 10
echo $(uptime) >> $MYDIR/$MYIP
else 
echo "mounted"
sleep 10
echo $(uptime) >> $MYDIR/$MYIP
fi
