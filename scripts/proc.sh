#!/bin/bash

#4. Написать скрипт, который контролирует выполнение какого-либо 
#процесса и в случае прерывания этого процесса возобновляет его работу. 
#Скрипт должен запросить периодичность проверки и имя процесса. 
#Скрипт должен иметь функции: создания, редактирования и удаления 
#заданий контроля. Сторонние задания cron, скрипт должен игнорировать
#(не показывать). Для выполнения задания НЕ разрешается создавать другие
# скрипты(скрипт должен запускаемый из крона и скрипт запускаемый 
#пользователем должен быть один.).


scriptname=$(echo "$(cd $(dirname $0) && pwd)/$(basename $0)")
script=$(echo "$(basename $0) ")
#echo "$scriptname "
print_menu () {
	echo "Перед вами управление процессами. Выберите желаемое действие:"
	echo "1 - повторный вывод этого меню"
	echo "2 - просмотр созданных заданий"
	echo "3 - создание нового задания"
	echo "4 - редактирование задания"
	echo "5 - удаление задания"
	echo "0 - выход из скрипта"
}
case $1 in
	s ) if [[ $(pgrep $2) -eq NULL ]]
		then $2
		else echo "Running"
		fi;;
	* ) 	
		exitt=0
		crontab -l > cron.bak
		print_menu

		while [[ $exitt -eq 0 ]]
		do
			read menu
			case $menu in
			 1 ) 	print_menu;;
			 2 )	grep -n $scriptname cron.bak;;
			 3 )	echo "min:  "
					read min
					echo "h:  "
					read hour
					echo "day of month:  "
					read daym
					echo "month:  "
					read month
					echo "day of week:  "
					read dayw
					echo "Filepath: "
					read filep
					echo "$min $hour $daym $month $dayw $scriptname s $filep" >> cron.bak
					sed '/./!d' cron.bak
					echo >> cron.bak
					crontab cron.bak
					crontab -l > cron.bak;;
			 4 )	grep -n $script cron.bak
				echo "Введите номер строки для редактирования: "
				read steditnum
				sed -n ""$steditnum"p" cron.bak
				read -ra str <<< "$(sed -n ""$steditnum"p" cron.bak)"
				m2exit=0
				min=${str[0]}
				hour=${str[1]}
				daym=${str[2]}
				month=${str[3]}
				dayw=${str[4]}
				filep=${str[7]}
				echo "Редактирование: 1-минуты, 2-часы, 3-дни месяца, 4-месяцы, 5-дни недели, 6-путь файла, 0-завершить редактирование и внести изменения."
				while [[ $m2exit -eq 0 ]]
				do
				echo "Что вы хотите отредактировать?"
				read menu2
				case $menu2 in
				 1)	echo "min:  "
					read min;;
				 2)	echo "h:  "
					read hour;;
				 3)	echo "day of month:  "
					read daym;;
				 4)	echo "month:  "
					read month;;
				 5)	echo "day of week:  "
					read dayw;;
				 6)	echo "Filepath: "
					read filep;;
				 0) m2exit=1;;
				esac
				done
				sed -i ""$steditnum"d" cron.bak
				echo "$min $hour $daym $month $dayw $scriptname s $filep" >> cron.bak
				sed -i '/^$/d' cron.bak
				echo >> cron.bak
				crontab cron.bak
				crontab -l > cron.bak
				;;
				
			 5 )	echo "Найдены следующие задания: "
				grep -n $script cron.bak
				echo "Введите номер строки для удаления: "
				read strnum
				sed -i ""$strnum"d" cron.bak
				echo "DELITED"
				sed -i '/./!d' cron.bak
				echo >> cron.bak
				crontab cron.bak
				crontab -l > cron.bak
				;;
			 0 ) 	echo "Завершение работы."
				exitt=1;;
			esac
		done;;
esac
exit 0
