#!/bin/bash
#редактирование коммита
exit=1
while [ "$exit" -eq "1" ]
do
	echo
	echo "Вы находитесь в:"
	pwd
	echo
	echo "Выберите действие:"
	echo "1 Репозиторий"
	echo "2 Файл"
	echo "3 Веткa"
	echo "4 Комит"
	echo "5 Работа с удаленным репозиторием"
	echo "6 Статус"
	echo "7 Сменить директорию"
	echo "8 Список"
	echo "0 Выход"
	read doing 

	case $doing in
	1)	echo
		echo "Вы находитесь в:"
		pwd
		echo
		echo "Выберите действие:"
		echo "1 Создание репозитория"
		echo "2 Удаление репозитория"
		echo "0 Назад "
		read doing1

		case $doing1 in
		
		1) git init;;
		2) rm -r .git;;
		0) echo;;
		*) echo "Введено неправильное действие";;		
		esac
	;;
	
	2)	echo "Выберите действие:"
		echo "1 Добавить/индексировать файл"
		echo "2 Удаление файла c диска"
		echo "3 Исключение файла из отслеживания"
		echo "4 Исключить директорию из отслеживания"
		echo "8 Список"
		echo "9 Назад "
		read doing1

		case $doing1 in
		1) echo "Введите имя файла"
		read filename
		git add $filename;;
		2) echo "Введите имя файла"
		read filename
		git rm -f $filename
		git reset HEAD $filename;;
		3) echo "Введите имя файла"
		read filename
		git rm --cached $filename;;
		3) echo "Введите имя файла"
		read dirname
		git rm --cached -r $direname;;
		8) echo
		ls;;
		0) ;;
		*) echo "Введено неправильное действие";;		
		esac
	;;

	3)
		echo "Выберите действие:"
		echo "1 Создание ветки"
		echo "2 Перейти на ветку"
		echo "3 Слияние веток"
		echo "4 Удаление ветки"
		echo "8 Список веток"
		echo "9 Назад "
		read doing1

		case $doing1 in
		
		1) echo "Введите название ветки"
		read brname
		git branch $brname;;
		2) echo "Введите название ветки"
		read brname
		git checkout $brname;;
		3) echo "Введите название ветки для слияния с текущей"
		read brname
		git merge $brname;;
		4) echo "Введите название ветки"
		read brname
		git branch -D $brname;;
		8) echo
		echo "Ветки:"
		git branch;;
		0) ;;
		*)echo "Введено неправильное действие";;		
		esac
	;;

	4)	echo "Выберите действие:"
		echo "1 Добавить"
		echo "2 Редактировать  коммит"
		echo "3 История логов"
		echo "0 Назад "
		read doing1

		case $doing1 in
		
		1) git commit;;
		2) git commit --amend;;
		3) git log
		ls;;
		0) ;;
		*) echo "Введено неправильное действие";;		
		esac
	;;

	5)	echo "Выберите действие:"
		echo "1 Добавить новую удалённую ссылку"
		echo "2 Отправить изменения на удаленный сервер"
		echo "3 Извлечь с удаленного сервера (fetch)"
		echo "4 Удалить ссылку"
		echo "5 Информация об удаленном сервере"
		echo "6 Отслеживать ветку на удаленном сервере (pull)"
		echo "7 Отобразить список удаленных ссылок"
		echo "0 Назад"
		read doing1

		case $doing1 in
		
		1) echo "Введите адрес:"
		read url
		echo "Введите имя:"
		read name
		git remote add $name $url;;
		2) echo "Введите имя удаленного репозитория:"
		read name
		echo "Введите имя ветки:"
		read brname
		git push --set-upstream $name $brname;;
		3) echo "Введите имя удаленного репозитория:"
		read name
		git fetch $name;;
		4) echo "Введите имя удаленного репозитория:"
		read name
		git remote rm $name;;
		5)echo "Введите имя удаленного репозитория:"
		read name
		git remote show $name;;
		6) ;;
		7) git remote;;
		0);;
		*)echo "Введено неправильное действие";;		
		esac
	;;

	
	6) git status;;
	8) echo
	ls;;
	7) echo "Введите директорию"
	read rname
	cd $rname;;
	0) exit 0;;
	*)echo "Введено нерпавильное действие"

	esac
done

