#!/bin/bash
source ./firewall.conf
#read -ra ins_pack <<< $(dpkg -l | awk '{print $2}')

i=1
while [ "${app[$i]}" ]
do
	if [[ -e $(which ${app[$i]}) ]]
	then
	iptables -A INPUT -s 0/0 -d localhost -p ${port[$i]} -j ACCEPT
	iptables -A OUTPUT -s localhost -d 0/0 -p ${port[$i]} -j ACCEPT
	fi
	i=$i+1
done
exit 0
