#!/bin/bash

#template [yyyy]-[mm]-[dd] [00-12]:[00-59]:[00-59]
read_time_pattern () {
	echo "year"
	read year
	if [ "$year" == "*" ]; then
		year='[0-9][0-9][0-9][0-9]'
	fi
	echo "month"
	read month
	if [ "$month" == "01" ]; then
		month_sym='Jan'
		month_rus='янв'
	fi
	if [ "$month" == "02" ]; then
		month_sym='Feb'
		month_rus='фев'
	fi
	if [ "$month" == "03" ]; then
		month_sym='Mar'
		month_rus='мар'
	fi
	if [ "$month" == "04" ]; then
		month_sym='Apr'
		month_rus='апр'
	fi
	if [ "$month" == "05" ]; then
		month_sym='May'
		month_rus='май'
	fi
	if [ "$month" == "06" ]; then
		month_sym='Jun'
		month_rus='июн'
	fi
	if [ "$month" == "07" ]; then
		month_sym='Jul'
		month_rus='июл'
	fi
	if [ "$month" == "08" ]; then
		month_sym='Aug'
		month_rus='авг'
	fi
	if [ "$month" == "09" ]; then
		month_sym='Sep'
		month_rus='сен'
	fi
	if [ "$month" == "10" ]; then
		month_sym='Okt'
		month_rus='окт'
	fi
	if [ "$month" == "11" ]; then
		month_sym='Nov'
		month_rus='ноя'
	fi
	if [ "$month" == "12" ]; then
		month_sym='Dec'
		month_rus='дек'
	fi
	if [ "$month" == "*" ]; then
		month='[0-1][0-9]'
		month_sym='[A-Z][a-z][a-z]'
		month_rus='[а-я][а-я][а-я]'
	fi
	echo "day"
	read day
	if [ "$day" == "*" ]; then
		day='[0-9][0-9]'
	fi
	echo "hour"
	read hour
	if [ "$hour" == "*" ]; then
		hour='[0-1][0-9]'
	fi
	echo "minute"
	read minute
	if [ "$minute" == "*" ]; then
		minute='[0-5][0-9]'
	fi
	echo "second"
	read second
	if [ "$second" == "*" ]; then
		second='[0-5][0-9]'
	fi
}

template_1 () {
	if [[ "$(grep -c "$year-$month-$day $hour:$minute:$second" $logfile)" -eq NULL ]]
	then echo "Log file dont have any notes for this time"
	else
	grep "$year-$month-$day $hour:$minute:$second" -a $logfile
	fi
	if [[ "$(grep "$month_sym $day $hour:$minute:$second" -c $logfile)" -eq NULL ]]
	then echo "Log file dont have any notes for this time"
	else
	grep "$month_sym $day $hour:$minute:$second" -a $logfile
	fi
	if [[ "$(grep "$year $month_rus $day $hour:$minute:$second" -c $logfile)" -eq NULL ]]
	then echo "Log file dont have any notes for this time"
	else
	grep "$year $month_rus $day $hour:$minute:$second" -a $logfile
	fi
}
template_2 () {
	if [[ "$(cat /var/log/"$proc"/* | grep -c "$day/$month_sym/$year:$hour:$minute:$second" )" -eq NULL ]]
	then echo "Log file dont have any notes for this time"
	else
	cat /var/log/"$proc"/* | grep "$day/$month_sym/$year:$hour:$minute:$second" -a
	fi
	if [[ "$(cat /var/log/"$proc"/* | grep -ca "$year-$month-$day  $hour:$minute:$second" )" -eq NULL ]]
	then echo "Log file dont have any notes for this time"
	else
	cat /var/log/"$proc"/* | sed -e '/./{H;$!d;}' -e "x;/$year-$month-$day  $hour:$minute:$second/!d;" 
	fi
}
template_3 () {
	if [[ "$(grep "$month_sym $day $hour:$minute:$second" /var/log/syslog | grep "$1" -c)" -eq NULL ]]
	then echo "Syslog dont have such notes"
	else
	echo "$proc"
	cat /var/log/syslog | grep "$proc"  | grep "$month_sym $day $hour:$minute:$second"
	fi
}
	logfile="/var/log/$1.log"
	proc="$1"
	#echo "$proc"
	#echo "$logfile"
	read_time_pattern
	if [ -f "/var/log/$proc.log" ]; then
	template_1
	else
	echo "there is no $logfile file"
	if [ -d "/var/log/$proc" ]; then
	template_2
	else
	echo "there is no /var/log/$proc directory"
	template_3
	fi
	fi
	
exit 0
