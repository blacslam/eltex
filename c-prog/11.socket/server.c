#include <ctype.h>
#include <pthread.h>
#include <stdio.h>      /* for printf() perror() and fprintf() */
#include <sys/socket.h> /* for socket() and bind() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_ntoa() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <unistd.h>     /* for close() */
#include <string.h>     /* for memset() */

pthread_mutex_t mine_lock = PTHREAD_MUTEX_INITIALIZER;

struct mine_str {
	int value;
	int sock;
	//int newsock;               /* Socket */
};

void DieWithError(char *errorMessage)
{
    perror(errorMessage);
    exit(1);
}

void* miner(void* args) {
	struct mine_str *mine = (struct mine_str *)args;
	printf("Остаток в шахте %d \n", mine->value);
	int newsock=mine->sock;
	int temp, time;
	if(newsock < 0)
    {
        perror("accept");
        exit(3);
    }
	for (;;) /* Run forever */
    {
		pthread_mutex_lock(&mine_lock);
		puts("mutex locked");
        send(newsock, &mine->value, sizeof(int), 0);
        printf("Объем шахты %d \n", mine->value);
        recv(newsock, &temp, sizeof(int), 0);
        if (temp<0) break;
        mine->value = temp;
        printf("Объем шахты %d \n", mine->value);
        recv(newsock, &time, sizeof(int), 0);
        pthread_mutex_unlock(&mine_lock);
        puts("Mutex unlocked");
        printf("thread waiting for %d sec\n", time);
        sleep(time);
    }
    pthread_mutex_unlock(&mine_lock);
    close(newsock);
	printf("Остаток в шахте %d \n Шахтер завершает работу. \n", mine->value);
	pthread_exit(NULL);
}

int main(int argc, char **argv){
	if (argc != 3)// Test for correct number of parameters
	{
        fprintf(stderr,"Usage:  %s <TCP SERVER PORT> <NUMBER OF MINERS>\n", argv[0]);
        exit(1);
    }
    int value, sock;
    pthread_t thr[100];
    //int sock, newsock;               /* Socket */
    struct sockaddr_in echoServAddr; /* Local address */
    unsigned short echoServPort;     /* Server port */
    struct mine_str mine;
	srand(getpid());
	value = rand()%100;
	mine.value=value;
	printf("Объем шахты %d \n", mine.value);
	printf("Ожидаем шахтеров...\n");
	echoServPort = (unsigned short)atoi(argv[1]);  /* First arg:  local port */
    if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
        DieWithError("socket() failed");
        /* Construct local address structure */
    memset(&echoServAddr, 0, sizeof(echoServAddr));   /* Zero out structure */
    echoServAddr.sin_family = AF_INET;                /* Internet address family */
    echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
    echoServAddr.sin_port = htons(echoServPort);      /* Local port */
    if (bind(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0)
        DieWithError("bind() failed");
    listen(sock, 1);
    int i=0;
	while((i<atoi(argv[2]))&&(mine.sock=accept(sock, NULL, NULL)))
	{
		puts("Connection accepted");
		if(pthread_create( &(thr[i]), NULL, miner, &mine) < 0)
        {
            perror("could not create thread");
            return 1;
        }
		i++;
	}
	close(sock);
	for (int i = 0; i < atoi(argv[2]); i++) {
		pthread_join(thr[i], NULL);
	}
    printf ("Выработка шахты более не возможна \n");
    printf("Объем шахты %d \n", mine.value);
	exit(0);
}
