#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h> 
#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), send(), and recv() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */

void DieWithError(char *errorMessage)
{
    perror(errorMessage);
    exit(1);
}

int main(int argc, char *argv[]) {
    int sock;                        /* Socket descriptor */
    struct sockaddr_in echoServAddr; /* Echo server address */
    unsigned short echoServPort;     /* Echo server port */
    char *servIP;                    /* Server IP address (dotted quad) */
	if (argc !=4) {
        printf("Usage: ./client <mine_vol> <Server IP> <Server_port> \n");
        exit(-1);
    }
    servIP = argv[2]; 
    echoServPort = atoi(argv[3]); /* Use given port */
    memset(&echoServAddr, 0, sizeof(echoServAddr));     /* Zero out structure */
    echoServAddr.sin_family      = AF_INET;             /* Internet address family */
    echoServAddr.sin_addr.s_addr = inet_addr(servIP);   /* Server IP address */
    echoServAddr.sin_port        = htons(echoServPort); /* Server port */
	if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
		DieWithError("socket() failed");
	/* Establish the connection to the echo server */
	if (connect(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0)
	{
		printf("Шахтер сообщает: Шахта закрыта\n");
		DieWithError("connect() failed");
	}
    int temp = 0;
	while (temp>=0) 
	{
		int temp = -1;
		recv(sock, &temp, sizeof(int), 0);
		printf("Шахтер рапортует: объем шахты %d \n", temp);
		printf("Шахтер рапортует: выработка %d \n", atoi(argv[1]));
		if (temp>=atoi(argv[1])) 
		{
			//printf("Шахтер рапортует: объем шахты %d \n", temp);
			temp-=atoi(argv[1]);
		}
		else
		{
			printf("Шахтер рапортует: объем шахты %d, выработка невозможна \n", temp);
			temp = -1;
			send(sock, &temp, sizeof(int), 0);
			break;
		}
		send(sock, &temp, sizeof(int), 0);
		int w_time=1+rand()%5;
		send(sock, &w_time, sizeof(int), 0);
		printf("Остаток в шахте %d \n", temp);
		printf("Шахтер ожидает %d секунд\n", w_time);
		sleep (w_time);
	}
	close(sock);
    exit(0); /* выход из процесс-потомока */
}
