#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[]) {
    int i, pid[argc], status, stat;
	if (argc < 2) {
        printf("Usage: ./fork_v6 v1 v2 ... vn \n");
        exit(-1);
    }
    for (i = 1; i < argc; i++) {
        // запускаем дочерний процесс 
        pid[i] = fork();
        if (-1 == pid[i]) {
            perror("fork"); /* произошла ошибка */
            exit(1); /*выход из родительского процесса*/
        } else if (0 == pid[i]) {
        	int matrix_element=0;
        	int vector_element=0;
        	int temp=0;
        	for (int j=1; j < argc; j++){
        		printf(" CHILD %d: Введите элемент %d строки матрицы \n", i, j);
            	scanf("%d", &matrix_element);
            	vector_element=atoi(argv[i]);
            	temp=temp+matrix_element*vector_element;
        	}
            printf(" CHILD %d: Итоговое значение элемента = %d \n", i, temp);
            exit(temp); /* выход из процесс-потомока */
        }
    }
    // если выполняется родительский процесс
    // ожидание окончания выполнения всех запущенных процессов
    for (i = 1; i < argc; i++) {
        status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) {
            printf("Элемент %d итогового вектора = %d\n", i, WEXITSTATUS(stat));
        }
    }
    return 0;
}
