#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <ctype.h>

union semun {
      int val;                  /* value for SETVAL */
      struct semid_ds *buf;     /* buffer for IPC_STAT, IPC_SET */
      unsigned short *array;    /* array for GETALL, SETALL */
                                /* Linux specific part: */
      struct seminfo *__buf;    /* buffer for IPC_INFO */
};



int main(int argc, char **argv){
	if (argc !=3 ) {
        printf("Usage: ./mine.exe miner_number mine_vol \n");
        exit(-1);
    }
	pid_t wpid;
	int status = 0;
	int pid[atoi(argv[1])];
	
	int shmid;
	key_t key = 69;
	int *shm;
	
	int semid;
	union semun arg;
	struct sembuf lock_res = {0, -1, 0};	//блокировка ресурса
	struct sembuf rel_res = {0, 1, 0};	//освобождение ресурса
	//int i, pid[argc], status, stat;
	srand(getpid());
	int mine_value = rand()%100;
	printf("Объем шахты %d \n", mine_value);
 /* Получим ключ, Один и тот же ключ можно использовать как
    для семафора, так и для разделяемой памяти */
	if ((key = ftok(".", 'S')) < 0) {
		printf("Невозможно получить ключ\n");
		exit(1);
	}
	/* Создадим семафор - для синхронизации работы с разделяемой памятью.*/
	semid = semget(key, 1, 0666 | IPC_CREAT);
	/* Установить в семафоре № 0 (Контроллер ресурса) значение "1" */
	arg.val = 1;
	semctl(semid, 0, SETVAL, arg);
	/* Создадим область разделяемой памяти */
	if ((shmid = shmget(key, sizeof(int), IPC_CREAT | 0666)) < 0) {
		perror("shmget");
		exit(1);
	}
	
	
	/* Получим доступ к разделяемой памяти */
				if ((shm = (int *)shmat(shmid, NULL, 0)) == (int *) -1) {
					perror("shmat");
					exit(1);
				}
				fflush(stdout);
				
				/* Заблокируем разделяемую память */	
				if((semop(semid, &lock_res, 1)) == -1){
					fprintf(stderr, "Lock failed\n");
					exit(1);
				} else{
					fflush(stdout);
				}
				
				/* Запишем в разделяемую память емкость */
				*(shm) = mine_value;
				 
				/* Освободим разделяемую память */
				if((semop(semid, &rel_res, 1)) == -1){
					 fprintf(stderr, "Unlock failed\n");
					 exit(1);
				} else{
					fflush(stdout);
				}
				
				 /* Отключимся от разделяемой памяти */
				if (shmdt(shm) < 0) {
					printf("Ошибка отключенияn");
					exit(1);
				}
	
	for (int i = 0; i < atoi(argv[1]); i++){
		pid[i] = fork();
			srand(getpid());
			if (0 == pid[i]) {
				printf("PID=%d i=%d\n", getpid(), i);
				fflush(stdout);
				if ((shm = (int *)shmat(shmid, NULL, 0)) == (int *) -1) {
					perror("shmat");
					exit(1);
				}
				if((semop(semid, &lock_res, 1)) == -1){
					fprintf(stderr, "Lock failed\n");
					exit(1);
				} else{
					printf("Semaphore resources decremented by one (locked) i=%d\n", i);
					fflush(stdout);
				}
				int temp = *(shm);
				if((semop(semid, &rel_res, 1)) == -1){
					 fprintf(stderr, "Unlock failed\n");
					 exit(1);
				} else{
					printf("Semaphore resources incremented by one (unlocked) i=%d\n", i);
					fflush(stdout);
				}
				if (shmdt(shm) < 0) {
					printf("Ошибка отключенияn");
					exit(1);
				}
				while (temp>0) {
					
				/* Получим доступ к разделяемой памяти */
				if ((shm = (int *)shmat(shmid, NULL, 0)) == (int *) -1) {
					perror("shmat");
					exit(1);
				}
				
				printf("Процесс ожидает PID=%d i=%d\n", getpid(), i);
				fflush(stdout);
				
				/* Заблокируем разделяемую память */	
				if((semop(semid, &lock_res, 1)) == -1){
					fprintf(stderr, "Lock failed\n");
					exit(1);
				} else{
					printf("Semaphore resources decremented by one (locked) i=%d\n", i);
					fflush(stdout);
				}
				
				/* Запишем в разделяемую память разность */
				if (*(shm)>atoi(argv[2])) {
					*(shm) = *(shm) - atoi(argv[2]);
					temp = *(shm);
					int w_time=rand()%5;
					printf("Остаток в шахте %d \n", temp);
					printf("Шахтер ожидает %d секунд\n", w_time);
					sleep (w_time);
				}
				else {
					temp=0;
				}
				 
				/* Освободим разделяемую память */
				if((semop(semid, &rel_res, 1)) == -1){
					 fprintf(stderr, "Unlock failed\n");
					 exit(1);
				} else{
					printf("Semaphore resources incremented by one (unlocked) i=%d\n", i);
					fflush(stdout);
				}
				
				 /* Отключимся от разделяемой памяти */
				if (shmdt(shm) < 0) {
					printf("Ошибка отключенияn");
					exit(1);
				}
			}
				exit(0);
			}	else if (pid[i]<0){
					perror("fork"); /* произошла ошибка */
					exit(1); /*выход из родительского процесса*/
			}
	}
	for (int i = 0; i < atoi(argv[1]); i++) {
		wpid = waitpid(pid[i], &status, 0);
		if (pid[i] == wpid) {
			printf("процесс-потомок %d done,  result=%d\n", i, WEXITSTATUS(status));
			fflush(stdout);
		}
	}	

	/* Получим доступ к разделяемой памяти */
	if ((shm = (int*)shmat(shmid, NULL, 0)) == (int*) -1) {
		perror("shmat");
		exit(1);
	}

	printf("------------------\n");
	printf("остаток в шахте=[%d]\n", *(shm));
	fflush(stdout);

	if (shmdt(shm) < 0) {
		printf("Ошибка отключенияn");
		exit(1);
	} 
	
	/* Удалим созданные объекты IPC */	
	 if (shmctl(shmid, IPC_RMID, 0) < 0) {
		printf("Невозможно удалить область\n");
		exit(1);
	} else
		printf("Shared memory segment marked for deletion\n");
	
	if (semctl(semid, 0, IPC_RMID) < 0) {
		printf("Невозможно удалить семафор\n");
		exit(1);
	}
}
