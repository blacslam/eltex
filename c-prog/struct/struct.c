#include <stdio.h>
#include <stdlib.h>
#include <string.h>


struct book{
	char name[50];
	unsigned int year;
	unsigned int pages;
	float price;
};
void printMenu() {
	printf("Выберите действие: v - вывод базы, s - сортировка, q - завершение работы, m - повторный вывод меню \n");
}
void readBooks(struct book *bk) {
	printf("Введите название книги: ");
	scanf("%19s", bk->name);
	printf("Введите год выпуска книги: ");
	scanf("%ud", &bk->year);
	printf("Введите количество страниц в книге: ");
	scanf("%ud", &bk->pages);
	printf("Введите стоимость книги: ");
	scanf("%f", &bk->price);
}
void printBooks(struct book *bk) {
	printf("%s \n", bk->name);
	printf("%d \n", bk->year);
	printf("%d \n", bk->pages);
	printf("%5.2f \n", bk->price);
}
static int cmp(const void *p1, const void *p2) {
	struct book * st1 = *(struct book**)p1;
	struct book * st2 = *(struct book**)p2;
	return strcmp(st1->name, st2->name);
}
int main(int argc, char* argv[]) {
	printf("Введите количество книг: ");
	int N=3;
	scanf("%d", &N);
	struct book** db = (struct book**)malloc(sizeof(struct book*)*N);

	for (int i = 0; i < N; i++) {
		db[i] = (struct book*)malloc(sizeof(struct book));

		readBooks(db[i]);
	}
	printMenu();
	char menu = 'm';
	int exit = 0;
	while (exit != 1) {
		while (getchar() != '\n');
		scanf("%c", &menu);
		switch ( menu )
		{
		case 'q':
			exit = 1;
			break;
		case 'm':
			printMenu();
			break;
		case 'v':
			printf("База содержит следующие записи: \n");
			for (int i = 0; i < N; i++) {
				printBooks(db[i]);
			}
			break;
		case 's':
			qsort(db, N, sizeof(struct book*), cmp);
			printf("База отсортирована.\n");
			break;
		default:
			printf("Неверный ввод \n");
			printMenu();
			break;
		}
	}
	for (int i = 0; i < N; i++)
	{
		free(db[i]);
	}
	free(db);
	return 0;
}
