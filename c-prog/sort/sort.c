#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int cmpstringp(const void *p1, const void *p2){
           /* Аргументами данной функции являются "pointers to
              pointers to char", однако аргументами strcmp(3)
              являются "pointers to char", так что здесь происходит
              преобразование и разыменовывание ссылок */
       	char **str1 = (char **)p1;
	char **str2 = (char **)p2;
	int cmp=strcmp(*str1, *str2);
	return -cmp;
}
/*void readarray (char* p) {
	p=(char *) malloc(100);
	scanf("%s",p);
}*/

int main(){
	int N;
	char *array [100];
	printf ("Введите количество строк: \n");
	scanf ("%d",&N);
	for (int i=0;i<N;i++){
		printf ("Введите строку: \n");
		array[i]=malloc(100);
		scanf("%s",array[i]);
//readarray (array[i]);
	}
	printf ("Строки после алфавитной сортировки: \n");
	int j;
	if (N < 2) {
		fprintf(stderr, "Usage: %s <string>...\n", array[0]);
		exit(EXIT_FAILURE);
	}
	qsort(&array[0], N, sizeof(char *), cmpstringp);
	for (j = 0; j < N; j++){
		puts(array[j]);
	}
	for (j=0; j<N; j++){
		free(array[j]);
	}
	exit(EXIT_SUCCESS);
}
