#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <pthread.h>

pthread_mutex_t mine_lock = PTHREAD_MUTEX_INITIALIZER;

struct mine_str {
	int value;
	int vol;
};

void* miner(void* args) {
	struct mine_str *mine = (struct mine_str *)args;
	printf("Остаток в шахте %d \n", mine->value);
	while (mine->value>=mine->vol) {
		pthread_mutex_lock(&mine_lock);
		mine->value = mine->value-mine->vol;
		int w_time=rand()%5;
		printf("Остаток в шахте %d \n", mine->value);
		pthread_mutex_unlock(&mine_lock);
		printf("Шахтер ожидает %d секунд\n", w_time);
		sleep (w_time);
	}
	pthread_mutex_unlock(&mine_lock);
	printf("Остаток в шахте %d \n Шахтер завершает работу. \n", mine->value);
	pthread_exit(NULL);
}

int main(int argc, char **argv){
	if (argc !=3 ) {
        printf("Usage: ./10.thr.exe miner_number mine_vol \n");
        exit(-1);
    }
	srand(getpid());
	struct mine_str mine;
	mine.value = rand()%100;
	mine.vol = atoi(argv[2]);
	pthread_t thr[atoi(argv[1])];
	printf("Объем шахты %d \n", mine.value);
	for (int i = 0; i < atoi(argv[1]); i++){
		int result = pthread_create(&(thr[i]), NULL, miner, &mine);
		if (result != 0) {
		perror("Creating the thread");
		return EXIT_FAILURE;
	}
	}
	for (int i = 0; i < atoi(argv[1]); i++) {
		pthread_join(thr[i], NULL);
	}
	printf("Объем шахты %d \n", mine.value);
	exit(0);
}
	
	
	
