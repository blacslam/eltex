#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>


#define MAX_SEND_SIZE 10

struct mymsgbuf {
		long mtype; /* тип сообщения, должен быть > О */ 
		char mtext[MAX_SEND_SIZE]; /* данные */

};

void ReadMtxString (int * MtxString, int n, int j) {
	printf ("N is %d; \n", n);
	for (int i=0; i<n; i++){
		printf(" CHILD %d: Введите элемент %d строки матрицы \n", j, i+1);
		scanf("%d", &MtxString[i]);
	}
}

int CountVectorElement (int * MtxString, int n, int Velem) {
	int temp=0;
	for (int i=0; i<n; i++){
		temp=temp+MtxString[i]*Velem;
	}
	return temp;
}
	
void send_message(int qid, struct mymsgbuf *qbuf, long type, char *text){
        qbuf->mtype = type;
        strcpy(qbuf->mtext, text);

        if((msgsnd(qid, (struct msgbuf *)qbuf,
                strlen(qbuf->mtext)+1, 0)) ==-1){
                perror("msgsnd");
                exit(1);
        }
}

int read_message(int qid, struct mymsgbuf *qbuf, long type){
        qbuf->mtype = type;
        msgrcv(qid, (struct msgbuf *)qbuf, MAX_SEND_SIZE, type, 0);
        int ret=atoi(qbuf->mtext);
        return ret;
}

int main(int argc, char **argv){
	int  pid[argc-1], status, stat, msgqid;
    key_t key;
    int qtype = 1;
    struct mymsgbuf qbuf;
	int * array;
	int vector [argc-1];
	key = ftok(".", 'm');
	if((msgqid = msgget(key, IPC_CREAT|0660)) == -1) {
		perror("msgget");
		exit(1);
	}
	for (int i=0; i<argc-1; i++){
		//pipe(fd[i]);
		
		pid[i] = fork();
		if (-1 == pid[i]) {
            perror("fork"); /* произошла ошибка */
            exit(1); /*выход из родительского процесса*/
        } else if (0 == pid[i]) {
            printf(" CHILD: Это %d процесс-потомок СТАРТ!\n", i+1);
            int vec;
            array=malloc(sizeof(int)*(argc-1));
			ReadMtxString (array, argc-1, i+1);
			vec=CountVectorElement(array,argc-1,atoi(argv[i+1]));
			printf("Элемент %d итогового вектора = %d\n", i+1, vec);
			free(array);
			printf(" CHILD: Это %d процесс-потомок ВЫХОД!\n", i+1);
            char str[MAX_SEND_SIZE];
            sprintf(str, "%d", vec);
            send_message(msgqid, (struct mymsgbuf *)&qbuf,
                                       qtype, str); 
			printf(" CHILD: Это %d процесс-потомок отправил сообщение!\n", i+1);
			fflush(stdout);
            exit(vec); /* выход из процесс-потомока */
		}

		status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) {
            printf("процесс-потомок %d done,  result=%d\n", i+1, WEXITSTATUS(stat));
			vector[i]=read_message(msgqid, &qbuf, qtype); 
        }
	}
	for (int i=0;i<argc-1;i++){
		printf("Элемент %d итогового вектора = %d\n", i+1, vector[i]);
		}
	return 0;
}
