#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h> 
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>


void ReadMtxString (int * MtxString, int n, int j) {
	printf ("N is %d; \n", n);
	for (int i=0; i<n; i++){
		printf(" CHILD %d: Введите элемент %d строки матрицы \n", j, i+1);
		scanf("%d", &MtxString[i]);
	}
}

int CountVectorElement (int * MtxString, int n, int Velem) {
	int temp=0;
	for (int i=0; i<n; i++){
		temp=temp+MtxString[i]*Velem;
	}
	return temp;
}

int main(int argc, char **argv){
	int  pid[argc-1], status, stat, buf;
	int fd[argc-1][2];
	int * array;
	int vector [argc-1];
	for (int i=0; i<argc-1; i++){
		pipe(fd[i]);
		pid[i] = fork();
		if (-1 == pid[i]) {
            perror("fork"); /* произошла ошибка */
            exit(1); /*выход из родительского процесса*/
        } else if (0 == pid[i]) {
            printf(" CHILD: Это %d процесс-потомок СТАРТ!\n", i+1);
            int vec;
            array=malloc(sizeof(int)*(argc-1));
			ReadMtxString (array, argc-1, i+1);
			vec=CountVectorElement(array,argc-1,atoi(argv[i+1]));
			printf("Элемент %d итогового вектора = %d\n", i+1, vec);
			free(array);
			close(fd[i][0]);
			write(fd[i][1], &vec, sizeof(int));
			printf(" CHILD: Это %d процесс-потомок ВЫХОД!\n", i+1);
            exit(vec);
		}

		status = waitpid(pid[i], &stat, 0);
        if (pid[i] == status) {
            printf("процесс-потомок %d done,  result=%d\n", i+1, WEXITSTATUS(stat));
            close(fd[i][1]);
            read(fd[i][0], &buf, sizeof(int));
            vector[i]=buf;
        }
	}
	for (int i=0;i<argc-1;i++){
		printf("Элемент %d итогового вектора = %d\n", i+1, vector[i]);
		}
	return 0;
}
